<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Tienda extends CI_Controller {
    
    //fun ExisteCarrito, ExisteProducto, ExisteStock
    
    
    //array que guarda los tipos de moneda
    public $divisas = [];
    
    
    public function __construct() {
        parent::__construct();
        
        
        /*MESSAGE: failed to open stream: HTTP request failed! HTTP/1.0 403 Forbidden 
        $XML = simplexml_load_file("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
        
        foreach($XML->Cube->Cube->Cube as $rate) {
            $this->divisas['divisa'][] = $rate["rate"];
            $this->divisas['valor'][] = $rate["currency"];
        }
        $this->divisas['divisa'][] = 'EUR';
        $this->divisas['valor'][] = 1;
        //sin objetos nuevos
        //estados pedidos: pedidos, enviados, recibidos, ¿anulados?
         * 
         */
    }
    
    public function index($inicio = 0) {
        $this->destacados($inicio);
    }
    
    public function destacados($inicio = 0) {
        $destacados = $this->Modelo_productos->listarDestacados($inicio, 8);
        $cont = $this->Modelo_productos->cantidadDestacados();
        
        $paginador = $this->paginacion(site_url("tienda/destacados/"), $cont);
        
        $lista = $this->load->view('lista_destacados', array('destacados' => $destacados), TRUE);
        
        $this->vista($paginador.$lista);
    }
    
    public function categorias($cat, $inicio = 0) {
        $datos = $this->Modelo_productos->buscarCategoria(array('nombre' => $cat));
        
        if($datos) {
            $productos = $this->Modelo_productos->listarXCategoria($datos['id'], $inicio, 8);
            $cont = $this->Modelo_productos->productosXCategoria($datos['id']);
            
            $paginador = $this->paginacion(site_url("tienda/categorias/"), $cont, 4);
            $lista = $this->load->view('lista_productos', array('productos' => $productos), TRUE);
            $this->vista($paginador.$lista);
        }
        else {
            if($cat = 'All') {
                $productos = $this->Modelo_productos->listar($inicio, 8);
                $cont = count($this->Modelo_productos->listar());
                
                $paginador = $this->paginacion(site_url("tienda/categorias/"), $cont, 4);
                $lista = $this->load->view('lista_productos', array('productos' => $productos), TRUE);
                $this->vista($paginador.$lista);
            }
            else {
                $lista = 'Categoría no disponible';
            }
            
            
            $this->vista($lista);
        }
    }
    
    public function producto($id) {
        $datos = $this->Modelo_productos->buscar($id);
        
        $cat = $this->Modelo_productos->buscarCategoria(array('id' => $datos['categoria_id']));
        
        $datos['categoria'] = $cat['nombre'];
        $lista = $this->load->view('vista_producto', array('producto' => $datos), TRUE);
        $this->vista($lista);
    }


    ########################
    
    //Añade paginacion
    public function paginacion($url, $total_filas, $seg = 3){
        $config['base_url'] = $url;
        $config['per_page'] = 8;
        $config['total_rows'] = $total_filas;
        $config['uri_segment'] = $seg;
        $config['full_tag_open'] = '<div class="paginacion">';
        $config['full_tag_close'] = '</div>';
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = '>';
        $config['prev_link'] = '<';
        $config['cur_tag_open'] = '<a href="'.site_url($this->uri->uri_string()).'">';
        $config['cur_tag_close'] = '</a>';
        
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
    
    //Muestra la vista completa
    public function vista($contenido) {
        $categorias = $this->Modelo_productos->listaCategorias();
        $cabecera = $this->load->view("cabecera", array("categorias" => $categorias) , TRUE);
        $datos = ["cabecera" => $cabecera, "contenido" => $contenido];
        
        $this->load->view('plantilla', $datos);
    }
    
    //TODO: funcion divisas
    /*public function divisa($divisa) {
    //  if($divisa == "EUR") {
    //      ;
    //  } else {
    //      ;
    //  }
    //}*/

    #######################
    
    //Calcula el precio final despues de aplicarle el descuento
    public function calcularPrecio($precio, $descuento = 0){
        $precio -= $precio * $descuento / 100;
        
        $precio = str_replace(".", ",", $precio);
        
        return $precio+" €";
    }
    
    public function agregarCarrito() {
        $id = $this->input->post('id');
        $cantidad = 1;
        $prod = $this->Modelo_productos->buscar($id);
        $stock = $this->input->post('cantidad');
        $descuento = $this->input->post('descuento');
        $precio = $this->input->post('precio');
        
        
        $carrito = $this->cart->contents();
        
        if(!empty($this->cart->contents())){
            foreach ($carrito as $producto) {
                if ($producto['rowid']['id'] == $id) {
                    $cantidad += $producto['rowid']['qty'];
                    $stock -= $producto['rowid']['qty'];
                    break;
                }
            }
        }
        
        $datos = array(
                        'id'      => $prod['id'],
                        'qty'     => $cantidad,
                        'price'   => $precio,
                        'name'    => utf8_encode($prod['nombre']),
                        'options' => array(
                            'stock' => $stock-1, 
                            'descuento' => utf8_encode($descuento), 
                            'precio_final' => $this->calcularPrecio($precio, $descuento)* $cantidad)
                    );
        
        if($this->cart->insert($datos)) {
            $mensaje = "Producto añadido";
        } else { $mensaje = "No se ha podido añadir el producto"; }
        
        $url = $this->input->post('url');
        
        redirect($url);
    }

    public function mostrarCarrito() {
        if($this->input->post()) {
            foreach ($this->input->post() as $id => $cantidad) {
                $data[] = array(
                    'rowid'=>$id,
                    'qty'=>$cantidad
                    );
            }
            $this->cart->update($data);
        }
        
        if($this->cart->contents()){
            $carrito['carrito'] = $this->cart->contents();
        } else {
            $carrito['carrito'] = "";
        }
        
        
        $lista = $this->load->view('vista_carrito', $carrito, TRUE);
        
        $this->vista($lista);
    }
    
    //Quita un producto del carrito
    public function quitarProducto($rowid) {
        $producto = array(
            'rowid' => $rowid,
            'qty' => 0
        );
        
        $this->cart->update($producto);
        redirect('tienda/mostrarCarrito');
    }
    
    //Borra el contenido del carrito
    public function vaciarCarrito() {
        $this->cart->destroy();
        
        redirect('tienda/mostrarCarrito');
    }
    
    //Añadimos los productos a las tablas pedido y linea_pedidos; despues borramos el carrito
    // y creamos un PDF
    public function finalizarCompra() {
        if(!$this->session->userdata('usuario')) {
            redirect(site_url('tienda/login'));
        } else {
            $usuario = $this->session->userdata('usuario');
            if($this->cart->total_items() != 0) {
                if($this->session->flashdata('total_final')){
                    $cliente = $this->Modelo_clientes->cliente($usuario);
                    $pedido = [
                        'dni' => $cliente['dni'],
                        'direccion' => $cliente['direccion'],
                        'nombre' => $cliente['nombre'],
                        'apellidos' => $cliente['apellidos'],
                        'fecha_pedido' => date('Y-m-d'),
                        'cliente_id' => $cliente['id']
                    ];
                    
                    $this->Modelo_pedidos->crear($pedido);
                    $id_pedido = $this->db->insert_id();
                    foreach ($this->cart->contents() as $producto) {
                        $linea_pedido = [
                            'precio_total' => $producto['options'] ['precio_final'],
                            'descuento' => $producto['options'] ['descuento'],
                            'cantidad' => $producto ['qty'],
                            'iva' => 21,
                            'producto_id' => $producto['id'],
                            'pedido_id' => $id_pedido
                        ];

                        $this->Modelo_pedidos->crearLineaPedido($linea_pedido);
                    }
                    
                    $this->cart->destroy();
                    $this->generarPDF($id_pedido, $this->cart->contents());
                    
                    
                    redirect('tienda/pedidosUsuario');
                }
            }
        }
    }


    ###################
    
    public function login() {
        //Seteamos reglas de validación para el login
        $this->form_validation->set_rules('usuario', 'Usuario', 'required|min_length[3]|max_length[45]|trim|xss_clean');
        $this->form_validation->set_rules('contr', 'Contraseña', 'trim|required|md5');
        
        if($this->input->post()) {
            $usuario = $this->input->post('usuario');
            $contr = $this->input->post('contr');
            if ($this->form_validation->run() == TRUE) {
                $datos = $this->Modelo_clientes->usuario($usuario, $contr);

                if ($datos != FALSE) {
                    if($datos['activo'] == 0) {
                        $this->Modelo_clientes->activar($usuario, $contr);
                    }
                    $this->session->set_userdata('usuario', $usuario);
                    redirect(site_url());  
                }
                else {
                    $this->session->set_flashdata('user', $usuario);
                    $mensaje = 'Datos erróneos. Por favor, inténtelo otra vez.';
                    $this->session->set_flashdata('mensaje', $mensaje);
                    $formulario = $this->load->view('form_acceso', 0, TRUE);
                    $this->vista($formulario);
                }
            }
            else {
                $this->session->set_flashdata('user', $usuario);
                $mensaje = 'Los datos no son válidos.';
                $this->session->set_flashdata('mensaje', $mensaje);
                $formulario = $this->load->view('form_acceso', 0, TRUE);
                $this->vista($formulario);
            }
        } else {
            //$cont = 0;
            //$this->session->set_flashdata('intentos', $cont);
            $formulario = $this->load->view('form_acceso', 0, TRUE);
            $this->vista($formulario);
        }
        
    }
    
    public function salir(){
        $this->session->unset_userdata('usuario');
        redirect("/tienda");
    }

    public function pedidosUsuario($inicio = 0) {
        $usuario = $this->session->userdata('usuario');
        $cliente = $this->Modelo_clientes->cliente($usuario);
        $pedidos = $this->Modelo_pedidos->listar($cliente['id'], $inicio, 8);
        $cont = $this->Modelo_pedidos->contar($cliente['id']);
        $productos = $this->Modelo_productos->listar();
        $lineas = [];
        
        for($i = 0; $i < 8; $i++){
            $lineas[$i] = $this->Modelo_pedidos->buscarLineaPedidos($pedidos[$i]['id']);
        }
        
        $paginador = $this->paginacion(site_url("tienda/pedidosUsuario/"), $cont);
        $lista = $this->load->view('lista_pedidos',array(
            'pedidos' => $pedidos, 'productos' => $productos, 'lineas' => $lineas), TRUE);
        $this->vista($paginador.$lista);
    }

    public function anularPedido($id){
        $url = $this->session->flashdata('url');
        $this->Modelo_pedidos->anular($id);
        
        redirect($url);
    }


    ##################
    
    public function generarXML() {
        $this->load->dbutil();
        $config = [
                'root'    => 'productos',
                'element' => 'producto',
                'newline' => "\n",
                'tab'    => "\t"
            ];
        $xml = $this->dbutil->xml_from_result($this->Modelo_productos->todo(),$config);
        $file_name = 'productos.xml';
        force_download($file_name, $xml);
        // Optionally redirect to the file you (hopefully) just created
        redirect("tienda"); 
    }
    
    //Genera el PDF del ultimo pedido
    public function generarPDF($pedido_id) {
        $pedido = $this->Modelo_pedidos->buscar($pedido_id);
        $this->session->set_userdata('pedido_id', $pedido_id);
        $linea_pedido = $this->Modelo_pedidos->buscarLineaPedidos($pedido_id);
        
        $this->Pdf = new Pdf($pedido);
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        
        $this->pdf->SetTitle("Recibo " . $pedido['id']);
        $this->pdf->SetLeftMargin(15);
        $this->pdf->SetRightMargin(15);
        $this->pdf->SetFillColor(200, 200, 200);
        $this->pdf->SetFont('Arial', 'B', 9);
        
        $x = 1;
        $subtotal = 0;
        $iva = 0;
        foreach ($linea_pedido as $linea) {
            $producto = $this->Modelo_productos->buscar($linea['producto_id']);
            $this->pdf->Cell(15, 7, $x++, 'BL', 0, 'C', '0');
            $this->pdf->Cell(85, 7, utf8_decode($producto['nombre']), 'B', 0, 'C', '0');
            $this->pdf->Cell(20, 7, $linea['precio_total'] . iconv('UTF-8', 'windows-1252', " €"), 'B', 0, 'C', '0');
            $this->pdf->Cell(20, 7, $linea['cantidad'], 'B', 0, 'C', '0');
            $this->pdf->Cell(20, 7, $linea['descuento'] . iconv('UTF-8', 'windows-1252', "%"), 'B', 0, 'C', '0');
            $total = ($linea['precio_total'] * $linea['cantidad'] - ($linea['precio_total'] * $linea['cantidad'] * ($linea['descuento'] / 100)));
            $subtotal += $total;
            $this->pdf->Cell(20, 7, round($total, 2) . iconv('UTF-8', 'windows-1252', " €"), 'BR', 0, 'C', '0');
            $this->pdf->Ln(7);
            $iva += $total * ($linea['iva'] / 100);
        }
        
        $this->pdf->Ln(7);
        $this->pdf->setX(155);
        $this->pdf->Cell(20, 7, "Subtotal", '', 0, 'R', '1');
        $this->pdf->Cell(20, 7, round($subtotal, 2) . iconv('UTF-8', 'windows-1252', " €"), 'B', 1, 'C', '0');
        $this->pdf->setX(155);
        $this->pdf->Cell(20, 7, "IVA", 'T', 0, 'R', '1');
        $this->pdf->Cell(20, 7, round($iva, 2) . iconv('UTF-8', 'windows-1252', " €"), 'B', 1, 'C', '0');
        $this->pdf->setX(155);
        $this->pdf->Cell(20, 7, "Total", 'TB', 0, 'R', '1');
        $this->pdf->Cell(20, 7, round($subtotal + $iva, 2) . iconv('UTF-8', 'windows-1252', " €"), 'B', 1, 'C', '0');
        $this->pdf->SetLink("");
        $this->pdf->Output(APPPATH . "../pdf/fact_" . $pedido['id'] . ".pdf", 'F');
        redirect('tienda/pedidosUsuario');
    }
}
