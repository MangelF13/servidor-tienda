<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of simplexml
 *
 * @author mange_000
 */
class Simplexml {
    //put your code here
    public function index() {
        //load the parser library
        $this->load->library('parser');
        $data['title'] = 'Parseando XML con la clase Simplexml de CodeIgniter';
        $data['products'] = $this->_getXML('holamundo');
        $this->parser->parse('table_view', $data);
    }

    public function _getXML($fname) {
        $result = array();
        $filename = $fname.'.xml';
        $xmlfile="./xml/".$filename;
        $xmlRaw = file_get_contents($xmlfile);
 
        $this->load->library('simplexml');
        $xmlData = $this->simplexml->xml_parse($xmlRaw);
 
        foreach($xmlData['item'] as $row) {
            $result .= '<tr><br>';
            $result .= '<td>'.$row['id'].'</td><br>';
            $result .= '<td>'.$row['name'].'</td><br>';
            //if (isset($row['@attributes'])) {
            //    $result .= '<td>'.
            //            $row['@attributes'].'</td><br>';
            //}
            $result .= '<td>'.$row['category'].'</td><br>';
            $result .= '<td>$ '.$row['price'].'</td><br>';
            $result .= '</tr>';
        }
        return $result;
    }
    
    
    
}