<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');






class Modelo_clientes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    
    public function usuario($usuario, $contr) {
        return $this->db->from('cliente')->where('usuario', $usuario)->where('contraseña', $contr)->get()->row_array();
    }
    
    public function cliente($usuario) {
        return $this->db->from('cliente')->where('usuario', $usuario)->get()->row_array();
    }
    
    
    public function activar($usuario, $contr, $activar = 1){
        $this->db->where('usuario', $usuario)->where('contraseña', $contr);
        $this->db->update('cliente', array('activo' => $activar));
        return TRUE;
    }

    

    /**
     * Inserta un cliente en la bdd
     * 
     * @param $opciones
     * 
     * @return mixed $consulta
     */
    public function registrar($opciones) {
        $this->db->insert('cliente', $opciones);
    }
    
    
    ########################
    
    
    public function listaProvincias() {
        $this->db->from('provincia');
        $consulta = $this->db->get();
        return $consulta->result_array();
    }
}
