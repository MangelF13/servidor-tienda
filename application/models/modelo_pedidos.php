<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Description of Modelo_pedidos
 *
 * @author Mangel
 */
class Modelo_pedidos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    //Crea pedido
    public function crear($pedido) {
        $this->db->insert('pedido', $pedido);
    }
    
    public function crearLineaPedido($linea_pedido){
        $this->db->insert('linea_pedidos', $linea_pedido);
    }
    
    public function buscar($pedido) {
        return $this->db->from('producto')->where('id', $pedido)->get()->row_array();
    }

    public function buscarLineaPedidos($id_pedido){
        return $this->db->from('linea_pedidos')->where('pedido_id', $id_pedido)->get()->result_array();
    }

    public function listar($cliente, $inicio = 0, $limit) {
        $this->db->select('p.*, sum(l.producto_id) as prod_totales, l.*')->
                from('pedido p, linea_pedidos l')->where('p.cliente_id', $cliente)->group_by('p.id')->order_by("fecha_pedido", "asc");
        if($limit != NULL){
            $this->db->limit($limit, $inicio);
        }
        return $this->db->get()->result_array();
    }
    
    public function contar($cliente) {
        return $this->db->from('pedido')->where('cliente_id', $cliente)->get()->num_rows();
    }


    public function anular($pedido_id) {
        $this->db->where('estado', 'P')->where('id', $pedido_id);
        $this->db->update('pedido', array('estado' => 'A'));
    }
}
