<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Modelo_productos extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function listar($inicio = 0, $limit = NULL) {
        $this->db->from('producto')->order_by("codigo", "asc");
        if($limit != NULL){
            $this->db->limit($limit, $inicio);
        }
        return $this->db->get()->result_array();
    }
    
    public function buscar($id) {
        return $this->db->from('producto')->where('id', $id)->get()->row_array();
    }
    
    public function cantidad() {
        return $this->db->count_all('producto');
    }
    
    ##############################
    
    public function listaCategorias() {
        return $this->db->from('categoria')->get()->result_array();
    }
    
    public function listarXCategoria($categoria, $inicio = 0, $limit) {
        $this->db->limit($limit, $inicio);
        return $this->db->select('p.*, c.nombre as cat_nombre, c.id as categoria')->from('producto p, categoria c')->
                where('p.categoria_id = c.id')->where('p.categoria_id', $categoria)->order_by("c.id", "asc")->get()->result_array();
    }
    
    public function productosXCategoria($cat) {
        return $this->db->from('producto')->where('categoria_id', $cat)->get()->num_rows();
    }
    
    function buscarCategoria($datos){
        return $this->db->where($datos)->order_by("id", "asc")->get('categoria')->row_array();
    }
    
    ##############################
    
    public function listarDestacados($start = 0, $limit = 5) {
        return $this->db->where('destacado', 1)->order_by("codigo", "asc")->limit($limit, $start)->get('producto')->result_array();
    }
    
    public function cantidadDestacados() {
        return $this->db->from('producto')->where('destacado', 1)->get()->num_rows();
    }
    
    ##############################
    
    public function todo() {
       return $this->db->select('p.*, c.nombre as cat_nombre, c.id as cat_id, c.visible as vis_cat, c.descripcion as desc_cat'.
               ', c.anuncio as anuncio_cat, c.codigo as cod_cat')->from('producto p, categoria c')->
               where('p.categoria_id = c.id')->order_by("c.id", "asc")->get();
    }
}
