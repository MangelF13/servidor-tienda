<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>

<div class="logo">Tienda online</div>
<div class="menu">
<?php if(!$this->session->userdata('usuario')) : ?>
<div class='user login'><a href=''>Registrar</a><a href='<?= site_url('tienda/login')?>'>Login</a></div>
<?php else : ?>
<div class='user salir'><a href='<?= site_url('tienda/salir')?>'>Salir</a></div>
<div class="despl">
    <ul class="nav user xml">
        <li><span>Servicios de la base de datos</span>
            <ul>
                <li><a href='<?= site_url('tienda/generarXML')?>'>Exportar productos</a></li>
                <li><a href='<?= site_url('tienda/')?>'>Importar productos (no funciona)</a></li>
                
            </ul>
        </li>
    </ul>    
    <ul class="nav user ser">
        <li><span>Pedidos</span>
            <ul>
                <li><a href='<?= site_url('tienda/pedidosUsuario')?>'>Lista de pedidos</a></li>
                <li><a href='<?= site_url('tienda/generarPDF/')?>'>Descargar PDF del último pedido</a></li>
            </ul>
        </li>
    </ul>
</div>
<?php    
endif;
echo "<div class='cat'>";
echo "<a href='".base_url()."'>Inicio</a>";
?>
    <ul class="nav">
        <li><span>Categorías</span>
            <ul>
<?php    foreach ($categorias as $categoria){
        if(!empty($categoria["visible"])) {
            $cat = str_replace("-", " ", $categoria["nombre"]);
            echo '<li><a href="'.site_url("tienda/categorias/".$categoria["nombre"]).'">'.ucfirst($cat).'</a></li>';
        }
    }
    echo "<a href='".site_url("tienda/categorias/All")."'>Lista de todos los libros</a>";
    echo "</div>";
?>

        <div class='carrito'><a href="<?= site_url("tienda/mostrarCarrito/")?>"><img src="<?=base_url("img/carrito-online.png")?>"><span><?=$this->cart->total_items() != 0? $this->cart->total_items() : "" ?></span></a>
    </div>
</div>


