<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>

<div class="productos destacados">
    <?php

if(!empty($destacados)) {
    foreach ($destacados as $producto) {
        if(isset($producto['visible']) && $producto['destacado'] == 1 && 
                $producto['inicio_destacado'] < date("Y-m-d H:i:s") && 
                ($producto['final_destacado'] > date("Y-m-d H:i:s") || $producto['final_destacado'] == NULL)) {
            echo "<div class='producto'>";
            echo "<a href='".  site_url("tienda/producto/".$producto['id'])."'><img src='".base_url($producto['imagen'])."' />";
            echo "<div class='nombre'>".$producto['nombre']."</div></a>";
            if(isset($producto['descuento'])) {
                echo "<div class='tachado'>".$producto['precio']." €</div>";
                echo "<div><b>".$producto['descuento']."% de descuento</b></div>";
            } else {
                echo "<div>".$producto['precio']." €</div>";
            }
            echo "<div>".$producto['autor']."</div>";

            if($producto['cantidad'] != 0) {
                echo form_open('tienda/agregarCarrito');
                echo "<input type='hidden' name='id' value='".$producto['id']."'/>";
                echo "<input type='hidden' name='precio' value='".$producto['precio']."'>";
                echo "<input type='hidden' name='url' value='".current_url()."'/>";
                echo "<input type='hidden' name='descuento' value='".$producto['descuento']."'/>";
                echo "<input type='hidden' name='cantidad' value='".$producto['cantidad']."'/>";
                echo "<input id='submit' name='submit' type='submit' value='Añadir al carro'/>";
                echo form_close("</div>");
            }
            else {
                echo "<div class='adv'>No quedan libros en stock</div>";
                echo "</div>";
            }
        }
    }
} 
else {
    echo "<div class='no productos'>No quedan libros destacados en stock</div>";
}
?>
</div>
