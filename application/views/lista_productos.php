<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>

<div class="productos">
<?php

if(!empty($productos)) {
    //$i = 0;
    foreach ($productos as $producto) {
        if(!empty($producto['visible'])) {
            echo "<div class='producto'>";
            echo "<a href='".  site_url("tienda/producto/".$producto['id'])."'><img src='".base_url($producto['imagen'])."' />";
            echo "<div class='nombre'>".$producto['nombre']."</div></a>";
            if(isset($producto['descuento'])) {
                echo "<div class='tachado'>".$producto['precio']." €</div>";
                echo "<div><b>".$producto['descuento']."% de descuento</b></div>";
            } else {
                echo "<div>".$producto['precio']." €</div>";
            }
            echo "<div>".$producto['autor']."</div>";

            if($producto['cantidad'] != 0) {
                echo form_open('tienda/agregarCarrito');
                ?>
                <input type='hidden' name='id' value="<?=$producto['id']?>"/>
                <input type='hidden' name='precio' value="<?=$producto['precio']?>">
                <input type='hidden' name='url' value="<?=current_url()?>"/>
                <input type='hidden' name='descuento' value="<?=$producto['descuento']?>"/>
                <input type='hidden' name='cantidad' value="<?=$producto['cantidad']?>"/>
                <input id='submit' name='submit' type='submit' value='Anular'/>
            <?php    echo form_close("</div>");
            }
            else {
                echo "<div class='adv'>No quedan libros en stock</div>";
                echo "</div>";
            }//$i++;
        }

    }
        
    //if($i == 0)
        //echo "<div class='no productos'>No hay libros de este tipo que mostrar</div>";
} 
else {
    echo "<div class='no productos'>No quedan libros de esta categoría en stock</div>";
}
?>

</div>
