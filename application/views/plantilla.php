<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css-reset.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/estilo.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/desplegable.css'); ?>">
    <title>Tienda CI</title>
</head>
<body>
    <div class="contenedor">
        <?php 
        echo $cabecera;
        ?>
        <div class="cuerpo">
            <?php
            
            echo isset($mensaje)? $mensaje: "";
            echo "<br/>";
            echo $contenido;
            ?>
        </div>
    </div>
</body>
</html>