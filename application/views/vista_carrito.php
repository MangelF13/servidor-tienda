<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!empty($carrito)) {
    echo form_open('tienda/mostrarCarrito');
?>
<table class="carrito" border=1>
<tr>
    <th>Nombre</th>
    <th>Precio</th>
    <th>Cantidad</th>
    <th>Descuento</th>
    <th>Borrar</th>
</tr>    
<?php
$total = 0;
    foreach ($carrito as $producto) :?>
        <tr>
            <td><?= $producto['name']?></td>
            <td><?= $producto['price']?></td>
            <td><input class="cantidad" type="number" min="0" max="<?=$producto['options']['stock']?>" name="<?=$producto['rowid']?>" value="<?=$producto['qty']?>"/></td>
            <td style="text-align:center"><?= $producto['options']['descuento']!=NULL?$producto['options']['descuento']."%":"" ?> </td>
            <td><a href='<?= site_url("tienda/quitarProducto/".$producto['rowid']."/")?>'><img src="<?= base_url('img/basura.jpg')?>"></a></td>
        </tr>
    <?php
    
    $total += $producto['options']['precio_final'];
    endforeach;
?>
<tr class="total">
    <td>Total: </td>
    <td><?= $total?></td>
    <td colspan="3" style="text-align:right"><a class="vaciar" href="<?=site_url('tienda/vaciarCarrito')?>">Vaciar carrito</a></td></td>
</tr>
</table>

<div class='finalizar'><a href="<?=site_url('tienda/finalizarCompra')?>">Finalizar la compra</a></div>
<?php 
$this->session->set_flashdata('total_final', $total);
echo form_close();
}
else {
    echo "El carrito está vacío";
}

