<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

?>
<div>
    <?php
    echo "<div class='prod'>";
    echo "<div class='producto'><img src='".base_url($producto['imagen'])."'>";
    echo "<div class='nombre'>".$producto['nombre']."</div>";
    if(isset($producto['descuento'])) {
        echo "<div class='tachado'>".$producto['precio']."€</div>";
        echo "<div><b>".$producto['descuento']."% de descuento</b></div>";
    } else {
        echo "<div>".$producto['precio']."</div>";
    }
    if($producto['cantidad'] != 0) {
        echo form_open('tienda/agregarCarrito');
        echo "<input type='hidden' name='id' value='".$producto['id']."'>";
        echo "<input type='hidden' name='precio' value='".$producto['precio']."'>";
        echo "<input type='hidden' name='url' value='".current_url()."'>";
        echo "<input type='hidden' name='descuento' value='".$producto['descuento']."'>";
        echo "<input type='hidden' name='cantidad' value='".$producto['cantidad']."'>";
        echo "<input id='submit' name='submit' type='submit' value='Añadir al carro'>";
        echo form_close("</br>");
    }
    else {
        echo "<div class='adv'>No quedan libros en stock</div>";
    }
    echo "</br><div class='cat'>Categoría: ".ucfirst($producto['categoria'])."</div>";
    
    echo "</div>";
    echo "<div class='descripcion'>".$producto['descripcion']."</div>";
    echo "</div>";
    echo "<div class='volver'><a href='".$this->input->post('url')."'>Volver a la página anterior</a></div>";
    ?>
</div>
