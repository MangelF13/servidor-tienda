-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-06-2015 a las 03:38:09
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `tienda-envios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
`id` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `descripcion` varchar(90) DEFAULT NULL,
  `anuncio` text,
  `codigo` varchar(40) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`, `anuncio`, `codigo`, `visible`) VALUES
(1, 'ficcion', NULL, NULL, NULL, 0),
(2, 'fantasia', NULL, NULL, NULL, 0),
(3, 'historia', NULL, NULL, NULL, 1),
(4, 'divulgacion', NULL, NULL, NULL, 1),
(5, 'autoayuda', NULL, NULL, NULL, 1),
(6, 'ciencia-ficcion', NULL, NULL, NULL, 1),
(7, 'monografia', NULL, NULL, NULL, 0),
(9, 'biografia', NULL, NULL, NULL, 0),
(10, 'autobiografia', NULL, NULL, NULL, 0),
(11, 'juegos', NULL, NULL, NULL, 0),
(12, 'novela-satírica', NULL, NULL, NULL, 0),
(13, 'novela', NULL, NULL, NULL, 1),
(14, 'novela-política', NULL, NULL, NULL, 0),
(15, 'de-texto', NULL, NULL, NULL, 1),
(16, 'filosofia', NULL, NULL, NULL, 1),
(17, 'diccionario', NULL, NULL, NULL, 0),
(18, 'entretenimiento', NULL, NULL, NULL, 0),
(19, 'linguistico', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_spanish2_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
`id` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `contraseña` char(32) NOT NULL COMMENT 'Para md5',
  `dni` char(9) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `c_postal` int(5) NOT NULL,
  `c_electronico` varchar(45) NOT NULL,
  `direccion` varchar(80) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `provincia_id` char(2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `usuario`, `contraseña`, `dni`, `nombre`, `apellidos`, `c_postal`, `c_electronico`, `direccion`, `activo`, `provincia_id`) VALUES
(1, 'Admin', 'admin', '12121212A', 'Miguel', 'Candón', 21001, 'mangelc@yahoo.net', 'Avda alemania 28', 1, '21'),
(2, 'pepe', '1234', '12121212A', 'mangel', 'c13', 21001, 'mangelc', 'avda alemania 28 2ºA', 1, '21'),
(4, 'juan', '1234', '12121212A', 'mangel', 'c13', 21001, 'mangelc', 'avda alemania 28 2ºA', 1, '21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea_pedidos`
--

CREATE TABLE IF NOT EXISTS `linea_pedidos` (
`id` int(11) NOT NULL,
  `precio_total` decimal(20,2) NOT NULL,
  `descuento` int(2) DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '1',
  `iva` int(2) NOT NULL DEFAULT '21',
  `producto_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Volcado de datos para la tabla `linea_pedidos`
--

INSERT INTO `linea_pedidos` (`id`, `precio_total`, `descuento`, `cantidad`, `iva`, `producto_id`, `pedido_id`) VALUES
(1, '19.00', 5, 1, 21, 27, 2),
(2, '31.00', 0, 1, 21, 2, 2),
(3, '19.00', 5, 1, 21, 27, 2),
(4, '19.00', 5, 1, 21, 27, 3),
(5, '31.00', 0, 1, 21, 2, 3),
(6, '19.00', 5, 1, 21, 27, 3),
(7, '19.00', 5, 1, 21, 27, 4),
(8, '31.00', 0, 1, 21, 2, 4),
(9, '19.00', 5, 1, 21, 27, 4),
(10, '19.00', 5, 1, 21, 27, 5),
(11, '31.00', 0, 1, 21, 2, 5),
(12, '19.00', 5, 1, 21, 27, 5),
(13, '19.00', 5, 1, 21, 27, 6),
(14, '31.00', 0, 1, 21, 2, 6),
(15, '19.00', 5, 1, 21, 27, 6),
(16, '19.00', 5, 1, 21, 27, 7),
(17, '31.00', 0, 1, 21, 2, 7),
(18, '19.00', 5, 1, 21, 27, 7),
(19, '19.00', 5, 1, 21, 27, 8),
(20, '31.00', 0, 1, 21, 2, 8),
(21, '19.00', 5, 1, 21, 27, 8),
(22, '19.00', 5, 1, 21, 27, 9),
(23, '31.00', 0, 1, 21, 2, 9),
(24, '19.00', 5, 1, 21, 27, 9),
(25, '19.00', 5, 1, 21, 27, 10),
(26, '31.00', 0, 1, 21, 2, 10),
(27, '19.00', 5, 1, 21, 27, 10),
(28, '19.00', 5, 1, 21, 27, 11),
(29, '31.00', 0, 1, 21, 2, 11),
(30, '19.00', 5, 1, 21, 27, 11),
(31, '19.00', 5, 1, 21, 27, 12),
(32, '31.00', 0, 1, 21, 2, 12),
(33, '19.00', 5, 1, 21, 27, 12),
(34, '31.00', 0, 1, 21, 2, 13),
(35, '19.00', 0, 1, 21, 1, 13),
(36, '29.00', 10, 1, 21, 26, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
`id` int(11) NOT NULL,
  `dni` char(9) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'P',
  `direccion` varchar(60) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `fecha_pedido` datetime NOT NULL,
  `fecha_entrega` datetime DEFAULT NULL,
  `cliente_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `dni`, `estado`, `direccion`, `nombre`, `apellidos`, `fecha_pedido`, `fecha_entrega`, `cliente_id`) VALUES
(1, '12121212A', 'A', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(2, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(3, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(4, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(5, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(6, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(7, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(8, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(9, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(10, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(11, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(12, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1),
(13, '12121212A', 'P', 'Avda alemania 28', 'Miguel', 'Candón', '2015-06-17 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
`id` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `descuento` int(3) DEFAULT NULL,
  `imagen` varchar(80) DEFAULT NULL,
  `iva` int(2) NOT NULL DEFAULT '21',
  `descripcion` text,
  `anuncio` text,
  `visible` tinyint(1) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `codigo` varchar(40) DEFAULT NULL,
  `destacado` tinyint(1) NOT NULL,
  `inicio_destacado` datetime DEFAULT NULL,
  `final_destacado` datetime DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `autor` varchar(40) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `precio`, `descuento`, `imagen`, `iva`, `descripcion`, `anuncio`, `visible`, `cantidad`, `codigo`, `destacado`, `inicio_destacado`, `final_destacado`, `categoria_id`, `autor`) VALUES
(1, 'El imperio romano', '19.99', NULL, 'img/IMPROM.jpg', 21, 'La serie informalmente titulada «Historia Universal Asimov» reúne las obras dedicadas por el gran novelista y divulgador científico a la evolución política, cultural y material de la especie humana. EL IMPERIO ROMANO expone las grandes líneas de desarrollo histórico de esta potencia del mundo antiguo desde la proclamación d e Augusto como emperador hasta la caída del Imperio Romano de Occidente y la instauración de los reinos germánicos.', NULL, 1, 2, 'IMPROM-ASIMOV,ISAAC', 1, '2015-05-12 00:00:00', NULL, 3, 'Isaac Asimov'),
(2, 'Cosmos', '31.00', NULL, 'img/COS.jpg', 21, 'Cosmos trata de la ciencia en su contexto humano más amplio y explica cómo la ciencia y la civilización se desarrollan conjuntamente. La obra aborda también el tema de las misiones espaciales destinadas a explorar los planetas más próximos a la Tierra, del origen de la vida, de la muerte del Sol, de la evolución de las gala xias y de los orígenes de la materia, los soles y los mundos, así como también los más recientes descubrimientos sobre la vida fuera de la Tierra. Con más de doscientas cincuenta ilustraciones a todo color verdaderamente asombrosas. Cosmos está narrado con la proverbial habilidad de su autor para conseguir que las ideas científicas fuesen comprensibles y apasionantes. ﻿', NULL, 1, 40, 'COS-SAGAN,CARL', 1, '2015-05-12 00:00:00', NULL, 4, 'Carl Sagan'),
(3, 'En el nombre del padre', '26.75', NULL, 'img/NOMPAD.jpg', 21, 'En Octubre de 1989, Gerry Conlon era el primero de los cuatro de Gilford que abandonaba la Corte Británica tras haber permanecido quince años en en la prisión, injustamente acusado de asesinato. Autobiografía de Gerry Conlos en la que se basó la película del mismo nombre.', NULL, 1, 4, 'NOMPAD-CONLON,GERRY', 1, '2015-05-12 00:00:00', NULL, 10, 'Gerry Conlon'),
(4, 'Tratactus logico-philosophicus', '9.99', NULL, 'img/TRALOG.jpg', 21, 'Como señaló Bertrand Russell en el prólogo a la traducción inglesa de 1922, reproducido en esta edición, el Tractatus logico-philosophicus «merece por su intento, objeto y profundidad, que se le considere un acontecimiento de suma importancia en el mundo filosófico». Esta obra clave de Ludwig Wittgenstein (1889-1951), a la vez clara y difícil, crispada y rigurosa, ofrece en un lenguaje aforístico, digno de la mejor prosa alemana, una filosofía del lenguaje y de la matemática, una reflexión acerca de la naturaleza y de la actividad filosófica, y una concepción del mundo.', NULL, 1, 25, 'TRALOG-WITTGENSTEIN,LUDWIG', 1, '2015-06-01 00:00:00', NULL, 16, 'Ludwig Wittgenstein'),
(5, 'Los griegos', '19.99', NULL, 'img/GRI.jpg', 21, 'La serie informalmente titulada «Historia Universal Asimov» reúne las obras dedicadas por el gran novelista y divulgador científico a la evolución política, cultural y material de la especie humana. "Los griegos" examina el amplio periodo que se extiende desde los tiempos micénicos hasta la configuración de la actual Grecia ya en el siglo xx, subrayando los aspectos más destacados de una cultura que estableció los cimientos artísticos, filosóficos y políticos sobre los que se asienta la actual civilización occidental.', NULL, 1, 0, 'GRI-ASIMOV,ISAAC', 1, '2015-06-14 00:00:00', NULL, 3, 'Isaac Asimov'),
(6, 'Trilogía del imperio', '19.99', NULL, 'img/TRIIMP.jpg', 21, 'El imperio galáctico, roído por la corrupción y las intrigas políticas, se desmorona. El psicohistoriador Hari Seldon ha previsto su decadencia, y para tratar de mitigar la expansión del caos resuelve crear dos Fundaciones en los dos extremos opuestos del Universo para salvaguardar la civilización. Pero la aparición de un s er mutante con poderes paranormales amenaza el futuro de la humanidad.', NULL, 1, 2, 'TRIIMP-ASIMOV,ISAAC', 0, NULL, NULL, 6, 'Isaac Asimov'),
(7, '1984', '19.99', NULL, 'img/1984.jpg', 21, 'En el año 1984 Londres es una ciudad lúgubre en la que la Policía del Pensamiento controla de forma asfixiante la vida de los ciudadanos. Winston Smith es un peón de este engranaje perverso, su cometido es reescribir la historia para adaptarla a lo que el Partido considera la versión oficial de los hechos... hasta que decid e replantearse la verdad del sistema que los gobierna y somete.', NULL, 1, 2, '1984-ORWELL,GEORGE', 0, NULL, NULL, 14, 'George Orwell'),
(8, 'Astronomía, guía para el aficionado', '49.99', NULL, 'img/ASTGUI.jpg', 21, 'Este libro es un pequeño atlas astronómico y una guía ilustrada de los objetos y fenómenos que pueden observarse con los medios al alcance del aficionado: los principales planetas y satélites del Sistema Solar, los asteroides, cometas y meteoros, estrellas, cúmulos estelares, nebulosas y galaxias. El capítulo final está dedicado a la observación del cielo.', NULL, 1, 2, 'ASTGUI-RÜRKL,ANTONIN', 1, '2015-05-12 00:00:00', NULL, 10, 'Antonin Rürkl'),
(9, 'Ley de Enjuiciamiento Criminal 20ª Edición', '35.99', NULL, 'img/LEYENJ.jpg', 21, 'Esta nueva edición, actualizada a junio de 2009, incluye el texto completo de la Ley de Enjuiciamiento Criminal con sus últimas modificaciones, además de abundante legislación complementaria en materia del proceso penal.', NULL, 1, 2, 'LEYENJ-MONTERO_AROCA,JUAN', 0, NULL, NULL, 15, 'Juan Montero Aroca'),
(10, 'Escuela De Ajedrez', '19.99', NULL, 'img/ESCAJE.jpg', 21, 'Escrito por Antonio Gude, uno de los mayores expertos de nuestro país, este libro servirá de apoyo tanto al autodidacta como al profesor o instructor. Incluye 28 partidas,más de 600 diagramas y 15 preguntas y 20 ejercicios al final de cada capítulo para autoevaluar su nivel de aprovechamiento. ', NULL, 1, 51, 'ESCAJE-GUDE,ANTONIO', 0, '2015-05-12 00:00:00', NULL, 18, 'Antonio Gude'),
(11, 'Ortografía de la lengua española', '19.99', NULL, 'img/ORTLEN.jpg', 21, 'La nueva Ortografía de la lengua española presenta unas características propias que la hacen más sólida, exhaustiva, razonada y moderna que su predecesora de 1999. De vocación panhispánica, es una obra concebida y realizada desde la unidad y para la unidad de la lengua.', NULL, 1, 200, 'ORTLEN-REAL_ACADEMIA_ESPAÑOLA', 1, '2015-05-12 00:00:00', NULL, 19, 'Real Academia Española'),
(24, 'This book is the milk', '9.99', NULL, 'img/BOOMIL.jpg', 21, '¿Odias el inglés? ¿Llevas estudiando inglés toda tu vida y no consigues aprenderlo? Pues escucha: este libro es para ti. ', NULL, 1, 40, 'BOOMIL-ALONSO,ALBERTO.MOLLA,DAMIAN', 1, '2015-06-15 00:00:00', NULL, 5, 'Alberto Alonso y Damián Mollá'),
(25, 'Los orígenes de Roma', '19.00', 20, 'img/ORIROM.jpg', 21, 'Junto con Salustio y Tácito, Tito Livio es uno de los tres grandes historiadores romanos. De su ingente obra histórica en 142 libri, que abarcan desde la llegada de Eneas al Lacio hasta la muerte de Druso en el año 9 a.C., sólo se conserva la cuarta parte: los libros I-X y XXI-XLV. ', NULL, 1, 10, 'ORIROM-LIVIO,TITO', 0, NULL, NULL, 3, 'Tito Livio'),
(26, 'Los nueve libros de la historia', '33.01', 10, 'img/NUELIB.jpg', 21, 'Heródoto de Halicarnaso (en griego Ἡρόδοτος Ἁλικαρνᾱσσεύς) fue un historiador y geógrafo griego que vivió entre el 484 y el 425 a. C.\r\nVersión completa en Español. ', NULL, 1, 47, 'NUELIB-DE HALICARNASO,HERODOTO', 1, '2015-06-15 00:00:00', NULL, 3, 'Heródoto de Halicarnaso'),
(27, 'Historia de los Estados Unidos', '20.00', 5, 'img/HISEST.jpg', 21, NULL, NULL, 1, 20, 'HISEST-BEARD,CHARLES_AUSTIN', 1, '2015-06-16 00:00:00', NULL, 3, 'Charles Austin Beard'),
(28, '1177 a. C. El año en que la civilización se derrumbó', '20.00', 5, 'img/1177.jpg', 21, 'En el año 1177 antes de Cristo unos merodeadores de origen desconocido, los llamados “pueblos del mar”, llegaron a Egipto, tras causar destrucción y muerte por donde pasaban. Esto sucedía en el inicio de una época de colapso en que desaparecieron las grandes civilizaciones de la Edad del Bronce -egipcios, hititas, micénicos, troyanos, asirios…- en lo que Finkelnstein describe como “uno de los más misteriosos procesos de la historia de la humanidad”. ¿Cuál fue la causa de este cataclismo? Eric H. Cline, de la Universidad George Washington, un arqueólogo que ha investigado este tema durante más de veinte años, responde que el fracaso se debió a une serie de causas conectadas entre sí: invasiones, revueltas, terremotos, y, sobre todo, la ruptura de un sistema de relaciones en un mundo que había alcanzado un notable grado de globalización. Algo que nos recuerda los riesgos que amenazan hoy a nuestro propio mundo. Cline pone al alcance del lector medio los más recientes hallazgos de la investigación en un relato realmente apasionante.', NULL, 1, 20, '1177-H._CLINE,ERIC', 1, '2015-06-16 00:00:00', NULL, 3, 'Eric H. Cline'),
(29, 'La Rusia de los Zares', '20.00', 5, 'img/RUSZAR.jpg', 21, 'Recorrido cronológico que comprende los años de mayor esplendor de Rusia, desde el siglo XVI hasta la abdicación de Nicolás II tras la revolución rusa de 1917. Los orígenes de este fromidable imperio se remontan a la creación por el legendario Rurik de la primera entidad rusa conocida, en el año 857, si bien su esplendor ll egaría con los zares Iván el Terrible, Pedro I el Grande o Catalina la Grande. Es un compendio de historia nacional e internacional, con recorridos biográficos de primer orden. ', NULL, 1, 20, 'HISEST-MUÑOZ-ALONSO,ALEJANDRO', 1, '2015-06-16 00:00:00', NULL, 3, 'Alejandro Muñoz-Alonso'),
(30, 'La Revolución Francesa', '20.00', NULL, 'img/REVFRA.jpg', 21, 'Un planteamineto tan nuevo como necesario para comprender uno de los procesos más importantes del mundo moderno. Acontecimiento fundacional del mundo contemporáneo, la Revolución francesa ha visto cómo su historia pasaba de la apología progresista tradicional a la descalifi cación, hasta negar su propia existencia, en la r eacción conservadora de las últimas décadas del siglo pasado. Jean-Clément Martin, profesor emérito de la Universidad de París, nos ofrece ahora una revisión basada en las investigaciones de los últimos treinta años, donde la Revolución se nos presenta, no como la realización de un proyecto único, sino como el punto de encuentro de una serie de proyectos reformistas y utópicos que competían entre sí, en un país fragmentado por una serie de identidades regionales, religiosas y políticas. Lo cual ayuda a entender la complejidad de su trayectoria, que comenzó como un intento de revolución por arriba, iniciado por la monarquía hacia 1770, y acabó, treinta años más tarde, tras una etapa de violencia desatada, en las manos de un general carismático. Martin nos ayuda así a entender cómo y por qué la Revolución transformó profundamente, no sólo Francia, sino nuestro propio mundo. ', NULL, 1, 50, 'REVFRA-MARTIN,JEAN-CLÉMENT', 0, NULL, NULL, 3, 'Jean-Clément Martin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `id` char(2) NOT NULL,
  `nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `nombre`) VALUES
('01', 'Álava'),
('02', 'Albacete'),
('03', 'Alicante'),
('04', 'Almera'),
('05', 'Ávila'),
('06', 'Badajoz'),
('07', 'Balears (Illes)'),
('08', 'Barcelona'),
('09', 'Burgos'),
('10', 'Cáceres'),
('11', 'Cádiz'),
('12', 'Castellón'),
('13', 'Ciudad Real'),
('14', 'Córdoba'),
('15', 'Coruña (A)'),
('16', 'Cuenca'),
('17', 'Girona'),
('18', 'Granada'),
('19', 'Guadalajara'),
('20', 'Guipzcoa'),
('21', 'Huelva'),
('22', 'Huesca'),
('23', 'Jaén'),
('24', 'León'),
('25', 'Lleida'),
('26', 'Rioja (La)'),
('27', 'Lugo'),
('28', 'Madrid'),
('29', 'Málaga'),
('30', 'Murcia'),
('31', 'Navarra'),
('32', 'Ourense'),
('33', 'Asturias'),
('34', 'Palencia'),
('35', 'Palmas (Las)'),
('36', 'Pontevedra'),
('37', 'Salamanca'),
('38', 'Santa Cruz de Tenerife'),
('39', 'Cantabria'),
('40', 'Segovia'),
('41', 'Sevilla'),
('42', 'Soria'),
('43', 'Tarragona'),
('44', 'Teruel'),
('45', 'Toledo'),
('46', 'Valencia'),
('47', 'Valladolid'),
('48', 'Vizcaya'),
('49', 'Zamora'),
('50', 'Zaragoza'),
('51', 'Ceuta'),
('52', 'Melilla');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD UNIQUE KEY `usuario_UNIQUE` (`usuario`), ADD KEY `fk_cliente_provincia1_idx` (`provincia_id`);

--
-- Indices de la tabla `linea_pedidos`
--
ALTER TABLE `linea_pedidos`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_Linea_pedidos_producto1_idx` (`producto_id`), ADD KEY `fk_Linea_pedidos_pedido1_idx` (`pedido_id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD KEY `fk_pedido_cliente1_idx` (`cliente_id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_UNIQUE` (`id`), ADD KEY `fk_producto_categoria1_idx` (`categoria_id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `linea_pedidos`
--
ALTER TABLE `linea_pedidos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
ADD CONSTRAINT `fk_cliente_provincia1` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `linea_pedidos`
--
ALTER TABLE `linea_pedidos`
ADD CONSTRAINT `fk_Linea_pedidos_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_Linea_pedidos_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
ADD CONSTRAINT `fk_pedido_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
ADD CONSTRAINT `fk_producto_categoria1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
